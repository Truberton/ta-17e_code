#You are given a single string. You need to convert each character into it's ASCII value and add the odd numbers together.

#Example
#Input: ABC
#A,B,C → 65,66,67
#Take odd numbers
#65+67=132
print(sum(ord(i)for i in input("Insert string: ")if ord(i)%2))